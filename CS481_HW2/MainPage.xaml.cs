﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


// CS481 HW2 - Calculator App MainPage CS
// Feb 1, 2020
// Olaf Zielinski

namespace CS481_HW2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
		// the currentState counter is used to keep track of the state of the app.
		// state == 1 is the default state, where app is awaiting operand1 and operand2.
		// state == 2 is where two operands are in the memory, and the app is ready for
		// state == -1 is when the operand1 is the result of previous calculation,
		//          awaiting the operand2 for next calculation.
		// state == -2 is when the first operand is in the memory, and the app is waiting
		// for the second operand.
		// the currentState logic was borrowed from an example calculator app at
		// https://github.com/xamarin/mobile-samples/tree/master/LivePlayer/BasicCalculator


		int currentState = 1;  // by default, the state of the app is 1, ready for 2 operands.
        string mathOperator = null;   // this string will store the math operator like +,-,*,/. 
        double operand1, operand2; // the two doubles for the numbers to be calculated.

        public MainPage()
        {
            InitializeComponent();
            ClickedClear(this, null); // if the person clicks on the AC button,
                                      // the app will reverse to default by using this function.
        }

        // This function sets both the operands to 0, the current state to 1 (ready for operands)
        // and clears the value of the viewer to 0. 
		private void ClickedClear(object sender, EventArgs e)
		{
			operand1 = 0;
			operand2 = 0;
			currentState = 1;
			mathOperator = null;
			this.resultText.Text = "0";
		}

        // This function handles what happens when the user clicks on the number button 0-9.
        // If the app was in the default state (1),
            // it will save the clicked number as the operand1, show it in the viewer
        // If the app was in the awaiting sate (<0),
            // it will clear what's in the viewer and save the clicked number as the operand2.
		void ClickedNumber(object sender, EventArgs e)
		{
			Button button = (Button)sender;
			string clicked = button.Text;

			//if the state is <0, the calculator is awaiting 2nd operand
			//so it has to delete whatever was in the viewer, save it as 2nd op,
			//and change the state of the app back to positive, to either await instruction or =.
            
                if (this.resultText.Text == "0" || currentState < 0)
			    {
				    this.resultText.Text = "";
				    if (currentState < 0)
					    currentState *= -1;
			    }

			if (this.resultText.Text.Length < 11)
			{
				//show the value clicked (if two numbers, add them) 
				this.resultText.Text += clicked;

                //if there's more than one decimal in the resultText, parse it and save as operand.
			    double number;
			    if (double.TryParse(this.resultText.Text, out number))
			    {
				    this.resultText.Text = number.ToString("N0");
				    if (currentState == 1) // if state==1, then both operands needed.
				    {
					    operand1 = number;
				    }
				    else // otherwise, one operand is in memory, so save as second.
				    {
					    operand2 = number;
				    }
			    }
			}   
		}

        //This function handles what happens when user clicks one of the operands.
		void ClickedOperator(object sender, EventArgs e)
		{
            //if the user does not press =, but both operands and operator are ready, do calculation
            //and then show the result. 
			if (mathOperator != null)
			{
				var result = Calculate(operand1, operand2, mathOperator); // Use calculate function to do the math. Store in result
				this.resultText.Text = result.ToString();
				//this.resultText.Text = result.ToString(); // Show what's in the result on the resultText window.
				operand1 = result; // be ready for next operation, by copying the result into the 1st operand.
				currentState = -1; // and change the state to -1, to say I have one operand, awaiting second operand or operator. 
			}

			currentState = -2; //change state to -2, to show that the 1st operand is in the mem and ready for =.
			Button button = (Button)sender;
			string pressed = button.Text; //send the value of the button (*+-/) to pressed
			mathOperator = pressed; //store the operator in the mathoperator variable. 
		}

        //This function handles what happens when user clicks on the result (=) button.
		void ClickedResult(object sender, EventArgs e)
		{
			if (currentState == 2) //Only do this if two operands and operator are ready in the memory)
			{
				var result = Calculate(operand1, operand2, mathOperator); // Use calculate function to do the math. Store in result

				this.resultText.Text = result.ToString(); // Show what's in the result on the resultText window.
				operand1 = result; // be ready for next operation, by copying the result into the 1st operand.
				currentState = -1; // and change the state to -1, to say I have one operand, awaiting second operand or operator. 
			} 
		}


        //This function does the basic arithmetic operations on two operands and an operator.
		public static double Calculate(double value1, double value2, string mathOperator)
		{
			double result = 0; //initialize the result to 0, to avoid any trash data in the value.

			switch (mathOperator) //based on the operator, do the following.
			{
				case "+":
					result = value1 + value2;
					break;

				case "-":
					result = value1 - value2;
					break;

				case "×":
					result = value1 * value2;
					break;

				case "/":
					if (value2 == 0) // You can't divide by 0, so the result is 0. It could be infinity, but that's weird. 
					{
                        result = 0;
					    break;
                    }
					else //otherwise, divide the values
					{
						result = value1 / value2;
						break;
					}
				
			}


			//Since the space on the display is limited, the result is rounded to 8 numbers after period.
			Math.Round(result, 8); 
			return result;
		}

	}

}
